<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'LaptopController@index')->name('laptops.index');

Route::get('/create', 'LaptopController@create')->name('laptops.create');

Route::post('/laptops', 'LaptopController@store')
    ->name("laptops.store");


Route::get('/{laptop}', 'LaptopController@show')
    ->name('laptops.show');
Route::get('/{laptop}/edit', 'LaptopController@edit')->name('laptops.edit');
Route::put('{laptop}', 'LaptopController@update')->name('laptops.update');
Route::delete('{laptop}', 'LaptopController@destroy')->name('laptops.destroy');



Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
