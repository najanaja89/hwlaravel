<?php

namespace App\Http\Controllers;

use App\Models\Laptop;
use Illuminate\Http\Request;

class LaptopController extends Controller
{

//    protected function rules()
//    {
//        return [
//
//        ];
//    }

    public function index()
    {
        return view('index', [
            'laptops' => Laptop::all()
        ]);
    }

    public function create()
    {

        return view('create');
    }

    public function store(Request $request)
    {
        $laptop = new Laptop($request->except('_token'));
        $laptop->save();

        return redirect()->route('laptops.index');
    }


    public function show(Laptop $laptop)
    {
        return view('show',[
            'laptop'=>$laptop
        ]);
    }


    public function edit(Laptop $laptop)
    {
        //dd($laptop);
        return view('edit', [
            'laptop' =>$laptop
        ]);
    }


    public function update(Request $request, Laptop $laptop)
    {
        $data = $request->except("_token");
        $laptop->fill($data);
        $laptop->save();
        return redirect()->route('laptops.index');
    }

    public function destroy(Laptop $laptop)
    {

        $laptop->delete();
        return redirect()->route('laptops.index');
    }
}
