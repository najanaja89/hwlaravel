@extends("layouts.app")
@section("content")
    <form action="{{route('laptops.update', $laptop)}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <input type="text" name="model" placeholder="enter laptop model" class="form-control" value="{{$laptop->model}}">
        </div>
        <div class="form-group">
            <input type="number" name="hdd" placeholder="enter hdd capacity" class="form-control" value="{{$laptop->hdd}}">
        </div>
        <div class="form-group">
            <input type="number" name="ram" placeholder="enter ram capacity" class="form-control" value="{{$laptop->ram}}">
        </div>
        <div class="form-group">
            <input type="text" name="cpu" placeholder="enter cpu model" class="form-control" value="{{$laptop->cpu}}">
        </div>
        <button class="btn btn-info" type="submit">edit Laptop</button>
    </form>
@endsection
