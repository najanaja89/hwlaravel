@extends("layouts.app")
@section("content")

    <form action="{{route('laptops.store')}}" method="post">
        @csrf
        <div class="form-group">
            <input type="text" name="model" placeholder="enter laptop model" class="form-control">
        </div>
            <div class="form-group">
                <input type="number" name="hdd" placeholder="enter hdd capacity" class="form-control">
            </div>
            <div class="form-group">
                <input type="number" name="ram" placeholder="enter ram capacity" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" name="cpu" placeholder="enter cpu model" class="form-control">
            </div>
            <button class="btn btn-info" type="submit">Add Laptop</button>
    </form>
@endsection

