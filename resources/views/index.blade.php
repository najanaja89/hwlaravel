@extends('layouts.app')

@section('content')
    <div>
        <div>
            <a href="{{route('laptops.create')}}" class="btn btn-link">Create</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>Model Name</th>
                <th>HDD Capacity</th>
                <th>RAM Capacity</th>
                <th>CPU Model</th>
            </tr>
            </thead>
            <tbody>
            @foreach($laptops as $laptop)
            <tr>
                <td>{{$laptop->model}}</td>
                <td>{{$laptop->hdd}}</td>
                <td>{{$laptop->ram}}</td>
                <td>{{$laptop->cpu}}</td>
                <td><a href="{{route('laptops.edit',$laptop)}}" class="btn btn-link">Edit</a></td>
                <td><a href="{{route('laptops.show',$laptop)}}" class="btn btn-link">Details</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
