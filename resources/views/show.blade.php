@extends('layouts.app')

@section('content')
<div>
    <div>
        <a href="{{route('laptops.index')}}" class="btn btn-link">Main</a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>Model Name</th>
            <th>HDD Capacity</th>
            <th>RAM Capacity</th>
            <th>CPU Model</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$laptop->model}}</td>
            <td>{{$laptop->hdd}}</td>
            <td>{{$laptop->ram}}</td>
            <td>{{$laptop->cpu}}</td>
        </tr>
    </table>
</div>
<form action="{{route('laptops.destroy', $laptop)}}" method="post">
    @csrf
    @method("delete")
<button type="submit" class="btn btn-danger">Delete</button>
</form>
@endsection
